// Copyright (c) 2015-present Mattermost, Inc. All Rights Reserved.
// See LICENSE.txt for license information.

package ldap

import (
	"time"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/shared/mlog"
)

const (
	jobName        = "LdapSync"
	schedFrequency = time.Hour
)

type Scheduler struct {
	lp *LDAPClient
}

func (i *LDAPClient) MakeScheduler() model.Scheduler {
	mlog.Debug("ldap.MakeWorker()")
	return &Scheduler{i}
}

func (scheduler *Scheduler) Name() string {
	return jobName + "Scheduler"
}

func (scheduler *Scheduler) JobType() string {
	return model.JobTypeLdapSync
}

func (scheduler *Scheduler) Enabled(cfg *model.Config) bool {
	return *scheduler.lp.settings.Enable
}

func (scheduler *Scheduler) NextScheduleTime(cfg *model.Config, now time.Time, pendingJobs bool, lastSuccessfulJob *model.Job) *time.Time {
	nextTime := time.Now().Add(schedFrequency)
	return &nextTime
}

func (scheduler *Scheduler) ScheduleJob(cfg *model.Config, pendingJobs bool, lastSuccessfulJob *model.Job) (*model.Job, *model.AppError) {
	data := map[string]string{}

	job, err := (**(*scheduler.lp).jobs).CreateJob(model.JobTypeLdapSync, data)
	if err != nil {
		return nil, err
	}
	return job, nil
}
